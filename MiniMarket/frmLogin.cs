﻿using MiniMarket.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarket
{
    public partial class frmLogin : Form
    {
        UsuariosDB usuariosDB;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            usuariosDB = new UsuariosDB();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            if(usuariosDB.ValidarUsuario(txtUsuario.Text,txtPassword.Text))
            {
                frmHome home = (frmHome)this.Owner;
                home.IniciarSesion(txtUsuario.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show(
                    "Los datos ingresados son incorrectos!", 
                    "Inicio de Sesión", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
        }
    }
}
