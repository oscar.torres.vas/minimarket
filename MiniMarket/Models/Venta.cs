﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace MiniMarket.Models
{
    [Table("Venta")] // crea una tabla como base en la base de datos 
    internal class Venta
    {
        [Key]
        public int Id { get; set; }
        public string CodBarra { get; set; }
        public string Producto { get; set; }
        public string Categoria { get; set; }
        public int Cantidad { get; set;}
        public decimal Precio { get; set; }

        [NotMapped] // detiene los datos que se guardan en el mapeo de datos para almacer
        public decimal Subtotal { 
            get {
                return Cantidad * Precio;
            }
        }
    }
}
