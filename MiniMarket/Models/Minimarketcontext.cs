﻿using System;
using System.Collections.Generic;
using System.Data.Entity; // genera un contexto que relaciona la base de datos con sus clases que queremos relacionar
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarket.Models
{
    internal class Minimarketcontext : DbContext
    {
        public Minimarketcontext() : base ("MinimarketCS") 
        {
        
        }

        public DbSet<Producto> Producto { get; set; }   // representa las tablas que quiero enviar y mostrar
        public DbSet<Usuario> Usuario { get; set; } // representa las tablas que quiero enviar y mostrar  
        public DbSet<Venta> Venta { get; set; } // representa las tablas que quiero enviar y mostrar 

    }
}
