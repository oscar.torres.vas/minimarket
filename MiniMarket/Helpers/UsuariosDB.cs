﻿using MiniMarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarket.Helpers
{
    internal class UsuariosDB
    {
        //private List<Usuario> LstUsuarios { get; set; }
        private Minimarketcontext db = new Minimarketcontext();
        public UsuariosDB()
        {
            //InitUsuarios();
            //db.Usuario.AddRange(LstUsuarios);
            //db.SaveChanges();
        }

        //private void InitUsuarios()
        //{
        //    LstUsuarios = new List<Usuario>();
        //    LstUsuarios.Add(new Usuario { Id = 1, TipoUsuario = "Administrador", Nombre = "admin", Email = "admin@gmail.com", LastLogin = DateTime.Now, Password = "admin" });
        //    LstUsuarios.Add(new Usuario { Id = 2, TipoUsuario = "Usuario Normal", Nombre = "Maria", Email = "maria@mail.com", LastLogin = DateTime.Now, Password = "clave456" });
        //    LstUsuarios.Add(new Usuario { Id = 3, TipoUsuario = "Usuario Normal", Nombre = "Pedro", Email = "pedro@mail.com", LastLogin = DateTime.Now, Password = "clave789" });

        //}

        public void InsertUsuario(Usuario user)
        {
            db.Usuario.Add(user);
            db.SaveChanges();
        }

        public void RemoveUsuario(Usuario user)
        {
            db.Usuario.Remove(user);
            db.SaveChanges();
        }

        public bool RemoveUsuario(string email)
        {
            Usuario user = db.Usuario.Where(p => p.Email == email).FirstOrDefault();
            if (user != null)
            {
                db.Usuario.Remove(user);
            }

            return user != null;

        }

        public bool RemoveUsuario(int id)
        {
            Usuario user = db.Usuario.Where(p => p.Id == id).FirstOrDefault();
            if (user != null)
            {
                db.Usuario.Remove(user);
            }

            return user != null;
        }

        public Usuario GetUsuario(int id)
        {
            Usuario user = db.Usuario.Where(p => p.Id == id).FirstOrDefault();
            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }


        public Usuario GetUsuario(string NombreOrEmail)
        {
            Usuario user = db.Usuario.Where(p => p.Email == NombreOrEmail || p.Nombre == NombreOrEmail).FirstOrDefault();
            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public List<Usuario> FindUsuario(string query)
        {
            List<Usuario> user = db.Usuario.Where(p => p.Email.Contains(query) || p.Nombre.Contains(query) || p.TipoUsuario.Contains(query)).ToList();
            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public bool UpdateUsuario(Usuario user)
        {
            Usuario userDB = db.Usuario.Where(p => p.Id == user.Id).FirstOrDefault();
            if (userDB != null)
            {
                userDB = user;
            }

            return userDB != null;
        }

        public bool UpdatePassUsuario(int id, string password)
        {
            Usuario userDB = db.Usuario.Where(p => p.Id == id).FirstOrDefault();
            if (userDB != null)
            {
                userDB.Password = password;
            }

            return userDB != null;
        }

        public bool UpdatePassUsuario(string email, string password)
        {
            Usuario userDB = db.Usuario.Where(p => p.Email == email).FirstOrDefault();
            if (userDB != null)
            {
                userDB.Password = password;
            }

            return userDB != null;
        }

        //public bool ExistUsuario(string email)
        //{
        //    return db.Usuario.Where(u => u.Email == email).FirstOrDefault();
        //}

        //public bool ExistUsuario(int id)
        //{
        //    return db.Usuario.Exists(u => u.Id == id);
        //}

        public int GetLastUserId()
        {
            return db.Usuario.OrderByDescending(o => o.Id).FirstOrDefault().Id;
        }

        public bool ValidarUsuario(string usuario, string password)
        {
            return db.Usuario.Any(u => u.Nombre==usuario && u.Password==password);
        }



    }
}
