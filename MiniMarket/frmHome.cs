﻿using MiniMarket.Helpers;
using MiniMarket.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarket
{
    public partial class frmHome : Form
    {
        ProductosDB productosDB = new ProductosDB();
        List<Venta> ListVenta = new List<Venta>();
        public frmHome()
        {
            InitializeComponent();
        }

        private void frmHome_Load(object sender, EventArgs e)
        {
            
        }

        public void IniciarSesion(string user)
        {
            toolSesion.Text = "Activa";
            toolUser.Text = "Usuario Actual: " + user;
            mnuCerrarSesion.Enabled = true;
            mnuIniciarSesion.Enabled = false;
            mnuMantenedores.Enabled = true;
            btnNuevaVenta.Enabled = true;
           
        }

        private void mnuIniciarSesion_Click(object sender, EventArgs e)
        {
            frmLogin login = new frmLogin();
            login.Owner = this;
            login.ShowDialog();
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            txtCodigoBarras.Enabled = true;
            txtCantidad.Enabled = true;
            txtCodigoBarras.ResetText();
            txtProducto.ResetText();
            txtPrecio.ResetText();
            txtCantidad.Value = 1;
            btnCancelarVenta.Enabled = true;
            btnFinalizarVenta.Enabled=true;
            btnNuevaVenta.Enabled=false;
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Producto prod = productosDB.GetProducto(txtCodigoBarras.Text);
                if (prod != null)
                {
                    txtProducto.Text = prod.Nombre;
                    txtPrecio.Text = prod.Precio.ToString("C2");
                    txtCantidad.Select();
                }
            }
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Producto prod = productosDB.GetProducto(txtCodigoBarras.Text);
                if (prod != null)
                {
                    txtCodigoBarras.ResetText();
                    txtProducto.ResetText();
                    txtPrecio.ResetText();
                    txtCantidad.Value = 1;
                    txtCodigoBarras.Select();
                    Venta item = new Venta()
                    {
                        CodBarra = prod.CodBarra,
                        Cantidad = (int)txtCantidad.Value,
                        Categoria = prod.Categoria,
                        Precio = prod.Precio,
                        Producto = prod.Nombre
                    };
                    ListVenta.Add(item);
                    RefreshTable();
                    txtTotal.Text = ListVenta.Sum(s => s.Subtotal).ToString("C2");
                }
            }
        }

        private void RefreshTable()
        {
            gvProductos.DataSource = null;
            gvProductos.DataSource = ListVenta;
            gvProductos.Update();
            gvProductos.Refresh();
        }

        private void mnuCerrarSesion_Click(object sender, EventArgs e)
        {
            toolSesion.Text = "Desconmectado";
            toolUser.Text = "Usuario Actual: -";
            mnuCerrarSesion.Enabled = false;
            mnuIniciarSesion.Enabled = true;
            mnuMantenedores.Enabled = false;
            btnNuevaVenta.Enabled = false;
            txtCodigoBarras.Enabled = false;
            txtCantidad.Enabled = false;
            LimpiarDatos();
        }

        private void LimpiarDatos()
        {
            txtTotal.ResetText();
            ListVenta.Clear();
            RefreshTable();
            txtProducto.ResetText();
            txtPrecio.ResetText();
            txtCodigoBarras.ResetText();
            txtCantidad.Value = 1;

        }

        private void btnCancelarVenta_Click(object sender, EventArgs e)
        {
            LimpiarDatos();
            txtCodigoBarras.Enabled = false;
            txtCantidad.Enabled = false;
        }

        private void btnFinalizarVenta_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Gracias por su compra!", "Sistema Minimarket",MessageBoxButtons.OK, MessageBoxIcon.Information);
            LimpiarDatos();
            btnNuevaVenta.Enabled = true;
            btnCancelarVenta.Enabled = false;
            btnFinalizarVenta.Enabled = false;
            txtCodigoBarras.Enabled = false;
            txtCantidad.Enabled = false;
        }

        private void gvProductos_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void btnRemoverProducto_Click(object sender, EventArgs e)
        {
            if (gvProductos.CurrentCell.RowIndex>=0)
            {
                string codBarra = gvProductos.Rows[gvProductos.CurrentCell.RowIndex].Cells[0].Value.ToString();
                Venta item = ListVenta.Find(v=> v.CodBarra==codBarra);
                ListVenta.Remove(item);
                RefreshTable();
                txtTotal.Text = ListVenta.Sum(s => s.Subtotal).ToString("C2");

            }

        }

        private void txtCantidad_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoBarras_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}
